import java.net.MalformedURLException;
import java.net.URL;

/**
 * Main class for testing WordCounter Class
 * @author Chinatip Vichian 5710546551
 *
 */
public class Main {
	/** Main method */
	public static void main(String [] args){
		final String DICT_URL = "http://se.cpe.ku.ac.th/dictionary.txt";
		URL url = null;
		try {
			url = new URL( DICT_URL );
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WordCounter counter = new WordCounter();
		int wordcount = counter.countWords( url );
		int syllables = counter.getSyllableCount( );
		System.out.println("WordCount :" + wordcount);
		System.out.println("Syllables :" + syllables);
	}
}
