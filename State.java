/**
 * State interface help to identify which state is it in.
 * @author Chinatip  Vichian 5710546551
 *
 */
public interface State {
	/**
	 * Handler method help switch state and count syllables.
	 * @param c is char that come to check state
	 */
	public void handler (char c);
}
