import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Scanner;

/**
 * WordCounter class help counts word, syllables ,and total syllables.
 * @author Chinatip Vichian  5710546551
 *
 */
public class WordCounter {
	/** State used in switch state of char */
	State state;
	/** Integer that counts syllables in words */
	int syllables= 0;
	/** total syllables of passage that was put in to countWords parameter */
	int totalSyllables=0;

	/**
	 * Count words from instream
	 * @param instream is text or passage that come to this class to check words and total syllables.
	 * @return number of words from instream
	 */
	public int countWords(InputStream instream){
		int d=0;
		Scanner in = new Scanner(instream);
		while(in.hasNext()){
			String g = in.nextLine();
			d++;
			totalSyllables += countSyllables(in.next());
		}
		return d;
	}
	/**
	 * Count words from url
	 * @param url is location of the information that wanted to be count
	 * @return number of words from url
	 */
	public int countWords(URL url){
		InputStream in =  null;
		try {
			in = url.openStream( );
		} catch (IOException e) {

		}
		return countWords(in);
	}
	/**
	 * Get total syllables of information from countWords method
	 * @return number of total syllables
	 */
	public int  getSyllableCount( ) {
		return totalSyllables;
	}
	
	/**
	 * Count syllables of word1
	 * @param word1 is a word that come in order to count its syllables
	 * @return number of syllables of word1
	 */
	public int countSyllables(String word1 ){
		syllables =0;
		state = CONSONANTSTATE;
		String word = word1.toLowerCase();
		for(int i=0; i<word.length();i++){
			char c = word.charAt(i);
			if(  c == '-' && (i==0||i==word.length()-1)){
				state = NOTAWORD;
				syllables=0;
				break;
			}
			
			if(i==word.length()-1 && c=='e'){
				if((syllables ==0 ) )
					syllables++;
			}
			else
				state.handler(word.charAt(i));
		}

		return syllables;
	}

	/** State of vowel */
	State VOWELSTATE = new State(){
		/**
		 * Handler method help switch state and count syllables.
		 * @param c is char that come to check state
		 */
		@Override
		public void handler(char c) {
			if(isVowelE(c))
				state = VOWELSTATE;
			else if (isDash(c)){
				state = DASHSTATE;
			}
			
			else if(isConsonant(c)){
				state = CONSONANTSTATE;
			}
			else if(isApostrophe(c)){
				state = APOSTROPHESTATE;
			}
			else {
				state = NOTAWORD;
				syllables =0;
			}
		}

	};
	
	/** 
	 * Check whether c is a, y, i, o, u
	 * @param c is char that come to check 
	 * @return a) true if c is a, y, i, o, u
	 * b) false if it is not
	 */
	boolean isVowelY(char c){
		//		return "".indexOf(c)>0;
		return (c=='a'||c=='u'||c=='i'||c=='o'||c=='y');
	}
	/** 
	 * Check whether c is a, e, i, o, u
	 * @param c is char that come to check 
	 * @return a) true if c is a, e, i, o, u
	 * b) false if it is not
	 */
	boolean isVowelE(char c){
		return (c=='a'||c=='u'||c=='i'||c=='o'||c=='e');
	}
	
	/** State of E_First */
	State EFIRSTSTATE = new  State(){
		/**
		 * Handler method help switch state and count syllables.
		 * @param c is char that come to check state
		 */
		@Override
		public void handler(char c) {
			if(isVowelE(c)){
				state = VOWELSTATE;
				syllables++;
			}
			else if (isDash(c)){
				state = DASHSTATE;
			}
			else if(isConsonant(c)){
				state = CONSONANTSTATE;
				syllables++;
			}
			else if(isApostrophe(c)){
				state = APOSTROPHESTATE;
			}
			else {
				state = NOTAWORD;
				syllables =0;
			}
		}
	};
	/** 
	 * Check whether c is e
	 * @param c is char that come to check 
	 * @return a) true if c is e
	 * b) false if it is not
	 */
	public boolean isEFirst(char c){
		return c=='e';
	}
	/** State of dash */
	State DASHSTATE = new State(){
		/**
		 * Handler method help switch state and count syllables.
		 * @param c is char that come to check state
		 */
		@Override
		public void handler(char c) {
			if(isDash(c))
				state = DASHSTATE;
			else if(isVowelY(c)){
				state = VOWELSTATE;
				syllables++;
			}
			else if (isEFirst(c))
				state = EFIRSTSTATE;
			else if(isConsonant(c))
				state = CONSONANTSTATE;
			else if(isApostrophe(c))
				state = APOSTROPHESTATE;
			else {
				state = NOTAWORD;
				syllables =0;
			}
		}

	};
	/** 
	 * Check whether c is "-"
	 * @param c is char that come to check 
	 * @return a) true if c is "-"
	 * b) false if it is not
	 */
	boolean isDash(char c){
		return c=='-';
	}
	/** State of Consonant */
	State CONSONANTSTATE = new State(){
		/**
		 * Handler method help switch state and count syllables.
		 * @param c is char that come to check state
		 */
		@Override
		public void handler(char c) {
			if(isVowelY(c)){
				state = VOWELSTATE;
				syllables++;
			}
			else if(isConsonant(c))
				state = CONSONANTSTATE;
			
			else if (isDash(c))
				state = DASHSTATE;
			else if(isEFirst(c))
				state = EFIRSTSTATE;
			else if(isApostrophe(c))
				state = APOSTROPHESTATE;
			else{
				state = NOTAWORD;
				syllables =0;
			}

		}
	};
	/** 
	 * Check whether c is letters and not a, e, i, o, u, y
	 * @param c is char that come to check 
	 * @return a) true if c is letters and not a, e, i, o, u, y
	 * b) false if it is not
	 */
	boolean isConsonant(char c){
		return (Character.isLetter(c)&& !(c=='a'||c=='e'||c=='i'||c=='o'||c=='u'));
	}
	/** State of Apostrophe */
	State APOSTROPHESTATE = new State(){
		/**
		 * Handler method help switch state and count syllables.
		 * @param c is char that come to check state
		 */
		@Override
		public void handler(char c) {
			if(isApostrophe(c))
				state = APOSTROPHESTATE;
			else if(isVowelY(c)){
				state = VOWELSTATE;
				syllables++;
			}
			else if (isDash(c))
				state = DASHSTATE;
			else if(isConsonant(c))
				state = CONSONANTSTATE;
			else if(isEFirst(c))
				state = EFIRSTSTATE;
			else{
				state = NOTAWORD;
				syllables =0;
			}

		}

	};
	/** 
	 * Check whether c is " ' "
	 * @param c is char that come to check 
	 * @return a) true if c is " ' "
	 * b) false if it is not
	 */
	boolean isApostrophe(char c){
		return c=='\'';
	}
	/** State of not a word */
	State NOTAWORD = new State(){
		/**
		 * Handler method help switch state and count syllables.
		 * @param c is char that come to check state
		 */
		@Override
		public void handler(char c) {
		}
	};

}
